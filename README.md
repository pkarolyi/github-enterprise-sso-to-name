# GitHub enterprise SSO to name
Convert enterprise SSOs to real names on GitHub

## Installation
Install from Chrome Web Store

## Configuration
After installation click the extension icon then the "Options" menuitem. You
will need to fill all the fields for the extension to work.

## About
Replaces enterprise ids with names in GitHub enterprise. The change is purely
visual.

This extension is the rewrite of [this](https://github.com/iamkirkbater/github-sso-to-name).

## Contribution
Feel free to open issues or Merge Requests. If the need arises I will write a contributing guide
but for now just don't be an asshole and don't be stupid.

## License
This is shared under `GPL-3.0-or-later`. The icons are [from Flaticon](https://profile.flaticon.com/license/free).
