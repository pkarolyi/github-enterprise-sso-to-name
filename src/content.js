/* Copyright (C) 2019  Károlyi, Péter Márton | See LICENSE for the full notice */

async function getFromStorage() {
  return new Promise(resolve => chrome.storage.sync.get(resolve));
}

function regexFromPattern(pattern) {
  return new RegExp(pattern, 'g');
}

function getHeaders(username, token) {
  return new Headers({ 'Authorization': 'Basic ' + btoa(`${username}:${token}`) });
}

function getIds(regex) {
  return [...new Set(document.body.innerText.match(regex))];
}

async function fetchNames(api_url, headers, ids) {
  return ids.reduce(async (prevPromise, id) => {
    let prev = await prevPromise;
    const response = await fetch(`${api_url}/users/${id}`, { headers });
    const user = await response.json();
    prev[id] = { name: user.name, time_fetched: Date.now() };
    return prev;
  }, Promise.resolve({}));
}

async function getNames(api_url, headers, ids) {
  const { cached_names } = await new Promise(resolve => chrome.storage.local.get(resolve));
  const ids_without_cached_names = ids.filter(id =>
    !cached_names
    || !cached_names[id]
    || Date.now() - cached_names[id].time_fetched > 1000 * 60 * 60  // 1 hour
  );
  const names = { ...cached_names, ...await fetchNames(api_url, headers, ids_without_cached_names) };
  chrome.storage.local.set({ cached_names: names });
  return names;
}

function usernameToName(regex, ids, names) {
  document.querySelectorAll('*').forEach(element => {
    element.childNodes.forEach(node => {
      if (node.nodeType === Node.TEXT_NODE) {
        const currentText = node.nodeValue;
        if (currentText.match(regex)) {
          ids.forEach(id => {
            const newText = currentText.replace(id, names[id].name);
            if (newText && newText !== currentText) {
              element.replaceChild(document.createTextNode(newText), node);
            }
          });
        }
      }
    });
  });
}

async function main() {
  const { url, api_url, pattern, username, token } = await getFromStorage();
  if (window.location.href.includes(url)) {
    if (pattern) {
      const regex = regexFromPattern(pattern);
      const headers = getHeaders(username, token);
      const ids = getIds(regex);
      const names = await getNames(api_url, headers, ids);
      usernameToName(regex, ids, names);
    } else {
      console.warn('sso-to-name: Please add an ID pattern for this to work.')
    }
  }
}

document.addEventListener('pjax:end', main);
main();