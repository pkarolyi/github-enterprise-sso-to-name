/* Copyright (C) 2019  Károlyi, Péter Márton | See LICENSE for the full notice */

const url_input = document.getElementById('url');
const api_url_input = document.getElementById('api_url');
const pattern_input = document.getElementById('pattern');
const username_input = document.getElementById('username');
const token_input = document.getElementById('token');
const save_button = document.getElementById('save');

chrome.storage.sync.get(data => {
  url_input.value = data.url ? data.url : '';
  api_url_input.value = data.api_url ? data.api_url : '';
  pattern_input.value = data.pattern ? data.pattern : '';
  username_input.value = data.username ? data.username : '';
  token_input.value = data.token ? data.token : '';
});

save_button.addEventListener('click', () => {
  chrome.storage.sync.set({
    url: url_input.value,
    api_url: api_url_input.value,
    pattern: pattern_input.value,
    username: username_input.value,
    token: token_input.value
  });
  window.alert('Saved successfully');
});